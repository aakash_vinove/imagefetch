import React from 'react'
import PropTypes from 'prop-types'

const fabric = window.fabric

class Text extends React.Component {
 constructor() {
  super()
  this.state = {
   text: "Hello World"
  }
 }

 static propTypes = {
  canvas: PropTypes.object,

 }

 static defaultProps = {
  fontFamily: 'Delicious_500',
  left: 0,
  top: 0,
  fontSize: 80,
  fill: "#FF0000"  // Set text color to red
 }

 componentDidMount() {
  const Text = new fabric.IText(this.state.text)
  this.props.canvas.add(Text)
 }

 render() {
  return null
 }
}

export default Text
