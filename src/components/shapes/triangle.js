import React from 'react'
import PropTypes from 'prop-types'

const fabric = window.fabric

class Triangle extends React.Component {
  static propTypes = {
    canvas: PropTypes.object,
    
  }

  static defaultProps = {
   width: 20, height: 30, fill: 'green', left: 0, top: 0
  }

  componentDidMount() {
    const Triangle = new fabric.Triangle(this.props)
    this.props.canvas.add(Triangle)
  }

  render() {
    return null
  }
}

export default Triangle
