import React from 'react'
import { render } from 'react-dom'
import 'fabric-webpack'

import DesignCanvas from './designedCurve'
import Rect from './rect'
import Circle from './circle'
import Image from './images'
import Triangle from './triangle'
import Text from './text'
const Shape = (props) => (
 
  <div>
     {console.log(props)}
    <DesignCanvas>
      {props.showRectangle?
      <Rect width={100} height={100} fill="blue" />
      :<div></div>}
      {props.showCircle?
      <Circle radius={50} top={200} />
      :<div></div>}
      {
        props.showTriangle?
      <Triangle width={100} height={100}/>
      :<div></div>}
      <Image url={props.url} scale={0.2} top={100} height={2000} width={3000}/>
      {
        props.showText?
      <Text width={100} height={100}/>
      :<div></div>}
    </DesignCanvas>
  </div>
)

export default Shape
