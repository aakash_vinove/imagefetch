import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { GridList, GridTile } from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import ZoomIn from 'material-ui/svg-icons/action/zoom-in';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Shape from '../shapes/index'

class ImageResults extends Component {
  state = {
    open: false,
    currentImg: '',
    showCircle: false,
    showRectangle: false,
    showTriangle: false,
    showText:false
  };

  handleOpen = img => {
    this.setState({ open: true, currentImg: img });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  addCircle = () => {
    this.setState({ showCircle: true })
  }
  addRectangle = () => {
    this.setState({ showRectangle: true })
  }
  addTriangle = () => {
    this.setState({ showTriangle: true })
  }
  addText = () => {
    this.setState({ showText: true })
  }
  render() {
    let imageListContent;
    const { images } = this.props;

    if (images) {
      imageListContent = (
        <GridList cols={3}>
          {images.map(img => (
            <GridTile
              title={img.tags}
              key={img.id}
              subtitle={
                <span>
                  by <strong>{img.user}</strong>
                </span>
              }
              actionIcon={
                <button onClick={() => this.handleOpen(img.largeImageURL)}>
                  Add caption
                </button>
              }
            >
              <img src={img.largeImageURL} alt="" />
            </GridTile>
          ))}
        </GridList>
      );
    } else {
      imageListContent = null;
    }

    const actions = [
      <FlatButton label="Close" primary={true} onClick={this.handleClose} />
    ];

    return (
      <div>
        {imageListContent}
        <Dialog
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          {/* <img src={this.state.currentImg} alt="" style={{ width: '100%' }} /> */}
          <Shape url={this.state.currentImg}
            showCircle={this.state.showCircle}
            showRectangle={this.state.showRectangle}
            showTriangle={this.state.showTriangle}
            showText={this.state.showText} />
            
          <FlatButton style={{backgroundColor:"red"}} onClick={this.addCircle}>Add circle</FlatButton>
          <FlatButton style={{backgroundColor:"green"}}onClick={this.addRectangle}>Add rectangle</FlatButton>
          <FlatButton style={{backgroundColor:"blue "}}onClick={this.addTriangle}>Add Triangle</FlatButton>
          <FlatButton style={{backgroundColor:"orange"}}onClick={this.addText}>Add Text</FlatButton>
        </Dialog>
        


      </div>
    );
  }
}

ImageResults.propTypes = {
  images: PropTypes.array.isRequired
};

export default ImageResults;
